 
 @extends('layout.header')


@section('title')
    /portfolio
@endsection

 @section('konten')
   
 <!-- ======= My Resume Section ======= -->
 <section id="resume" class="resume">
    <div class="container">

      <div class="section-title">
        <span>My Resume</span>
        <h2>My Resume</h2>
        <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p>
      </div>

      <div class="row">
        <div class="col-lg-6">
          <h3 class="resume-title">Sumary</h3>
          <div class="resume-item pb-0">
            <h4>Dius Wahyu Aditya</h4>
            <p><em>A student who likes to play games and sell things online</em></p>
            <p>
            <ul>
              <li>Karangasem, Bali</li>
              <li>+62 8123 9962 713</li>
              <li>dius@undiksha.ac.id</li>
            </ul>
            </p>
          </div>

          <h3 class="resume-title">Education</h3>
          <div class="resume-item">
            <h4>Science</h4>
            <h5>2016 - 2019</h5>
            <p><em>Bebandem Senior High School</em></p>
            <p>Qui deserunt veniam. Et sed aliquam labore tempore sed quisquam iusto autem sit. Ea vero voluptatum qui ut dignissimos deleniti nerada porti sand markend</p>
          </div>
          <div class="resume-item">
            <h4>Informatics Engineering</h4>
            <h5>2019 - now</h5>
            <p><em>Ganesha University of Education</em></p>
            <p>Quia nobis sequi est occaecati aut. Repudiandae et iusto quae reiciendis et quis Eius vel ratione eius unde vitae rerum voluptates asperiores voluptatem Earum molestiae consequatur neque etlon sader mart dila</p>
          </div>
        </div>
        <div class="col-lg-6">
          <h3 class="resume-title">Experience</h3>
          <div class="resume-item">
            <h4>Deputy Chairman of the Integer Committee</h4>
            <h5>2020</h5>
            <p><em>Singaraja, Buleleng, Bali</em></p>
            <p>
            <ul>
              <li>Lead in the design, development, and implementation of the graphic, layout, and production communication materials</li>
              <li>Delegate tasks to the 7 members of the design team and provide counsel on all aspects of the project. </li>
              <li>Supervise the assessment of all graphic materials in order to ensure quality and accuracy of the design</li>
              <li>Oversee the efficient use of production project budgets ranging from $2,000 - $25,000</li>
            </ul>
            </p>
          </div>
          <div class="resume-item">
            <h4>Head of the IT MISSION #4 Committee</h4>
            <h5>2021</h5>
            <p><em>Singaraja, Buleleng, Bali</em></p>
            <p>
            <ul>
              <li>Developed numerous marketing programs (logos, brochures,infographics, presentations, and advertisements).</li>
              <li>Managed up to 5 projects or tasks at a given time while under pressure</li>
              <li>Recommended and consulted with clients on the most appropriate graphic design</li>
              <li>Created 4+ design presentations and proposals a month for clients and account managers</li>
            </ul>
            </p>
          </div>
        </div>
      </div>

    </div>
  </section><!-- End My Resume Section --> 
  @endsection