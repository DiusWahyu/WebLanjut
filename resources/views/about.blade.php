@extends('layout.header')


@section('title')
    about
@endsection

@section('konten')

<main id="main">
 <!-- ======= About Me Section ======= -->
 <section id="about" class="about">
    <div class="container">

      <div class="section-title">
        <span>About Me</span>
        <h2>About Me</h2>
        <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p>
      </div>

      <div class="row">
        <div class="image col-lg-4 d-flex align-items-stretch justify-content-center justify-content-lg-start"></div>
        <div class="col-lg-8 d-flex flex-column align-items-stretch">
          <div class="content ps-lg-4 d-flex flex-column justify-content-center">
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Name:</strong> <span>I Made Dius Wahyu Aditya</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span>www.idus.com</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>+62 81 237 833 261</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>Bali, Indonesia</span></li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Age:</strong> <span>20</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Rank:</strong> <span>Epic</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>PhEmailone:</strong> <span>dius"undiksha.ac.id</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Freelance:</strong> <span>Available</span></li>
                </ul>
              </div>
            </div>
            <div class="row mt-n4">
              <div class="col-md-6 mt-5 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="bi bi-emoji-smile" style="color: #20b38e;"></i>
                  <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" class="purecounter"></span>
                  <p><strong>Cheerful</strong>a person who is always grateful and happy</p>
                </div>
              </div>

              <div class="col-md-6 mt-5 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="bi bi-clock" style="color: #2cbdee;"></i>
                  <span data-purecounter-start="0" data-purecounter-end="18" data-purecounter-duration="1" class="purecounter"></span>
                  <p><strong>Years of experience</strong>
                    5th semester student of Informatics Engineering
                </p>
                </div>
              </div>

              <div class="col-md-6 mt-5 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="bi bi-award" style="color: #ffb459;"></i>
                  <span data-purecounter-start="0" data-purecounter-end="16" data-purecounter-duration="1" class="purecounter"></span>
                  <p><strong>Awards</strong> no awards </p>
                </div>
              </div>
            </div>
          </div><!-- End .content-->

          <div class="skills-content ps-lg-4">
            <div class="progress">
              <span class="skill">HTML <i class="val">100%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">CSS <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">JavaScript <i class="val">75%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="55" aria-valuemax="100"></div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section><!-- End About Me Section -->
</main><!-- End #main -->
@endsection